let pick = true

let currentPick

console.log("unchanged pick", pick)

function moveDisk(e) {
    let tower = e.currentTarget

    console.log(pick)

    if (pick) {
        currentPick = tower.lastElementChild
        document.getElementById('holder').appendChild(currentPick)
        pick = false
    } else if (pick === false) {
        if (!tower.lastElementChild) {
            tower.appendChild(currentPick)
            pick = true
        } else if (Number(currentPick.id) < Number(tower.lastElementChild.id)) {
            tower.appendChild(currentPick)
            pick = true
        }
    }

    if (document.getElementById('endTower').childElementCount === 4) {
        alert('You win!')
    }
}

document.getElementById('startTower').addEventListener('click', moveDisk)
document.getElementById('endTower').addEventListener('click', moveDisk)
document.getElementById('middleTower').addEventListener('click', moveDisk)